
module NORM

class SQL
   def initialize(i_db,i_sql)
      @db = i_db
      @sql = i_sql
   end
   def exec(*i_args)
      @db.norm.cursors.exec(@sql,i_args)
   end
end

end # module NORM

