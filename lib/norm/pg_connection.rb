
class PG::Connection
   def norm
      instance_variable_defined?('@norm_cursors') or
         @norm_cursors = NORM::Cursors.new(self)
      instance_variable_defined?('@norm_object') or
         @norm_object = NORM::NORM.new(self)

      @norm_object
   end
end

