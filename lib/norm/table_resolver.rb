
module NORM

class TableResolver < BasicObject
   def initialize(i_db)
      @db = i_db
   end
   def method_missing(i_method,*i_args,**i_nargs)
      if i_args.count == 0 and  i_nargs.count == 0
         Table.new(@db,i_method.to_s)
      else
         super
      end
   end
end

end # module NORM


