
module NORM

class Cursors
   def initialize(i_db)
      @db = i_db
      @cursors = {}
      @cursor_id = 0
   end
   def exec(i_sql,i_args)
      if !@cursors.has_key?(i_sql)
         t_cursor_id = next_cursor_id
         @db.norm.logger.info "(NORM) preparing #{t_cursor_id} - #{i_sql}"
         @db.prepare("#{t_cursor_id}",i_sql)
         @cursors[i_sql] = t_cursor_id
      end
      t_cursor_id = @cursors[i_sql]
      @db.norm.logger.info "(NORM) executing #{t_cursor_id} - #{i_sql}"
      @db.norm.logger.debug "(NORM) args: #{i_args.inspect}"
      @db.exec_prepared(t_cursor_id,i_args)
   end
   def next_cursor_id
      "norm-#{@cursor_id}".tap { @cursor_id += 1 }
   end
end

end # module NORM

