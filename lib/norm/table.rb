
module NORM

class Table
   attr_reader :db, :name
   def initialize(i_db,i_name)
      @db = i_db
      @name = i_name
   end
   def select(*i_columns,**i_nargs)
      t_where = i_nargs.delete(:where) || {}
      t_sql =
         'select ' +
         i_columns.join(', ') +
         " from #{name}"
      if t_where && t_where.count > 0
         t_sql << 
            ' where ' +
            t_where.keys.collect.with_index { |n,i|
               "#{n} = $#{i+1}"
            }.join(' AND ')
      end
      db.norm.cursors.exec(t_sql,t_where.values)
   end
   def insert(**i_nargs)
      t_sql =
         "insert into #{name}" +
         "(" +
         i_nargs.keys.join(',') +
         ") " +
         "values(" +
         i_nargs.count.times.collect.with_index { |_,i| "$#{i+1}" }.join(',') +
         ")"
      db.norm.cursors.exec(t_sql,i_nargs.values)
   end
   def update(**i_args)
      t_where = i_args.delete(:where)
      t_set = i_args.keys.collect.with_index { |n,i| "#{n} = $#{i+1}" }
      t_sql =
         "update #{name}" +
         " set " +
         t_set.join(',') +
         ' where ' +
         t_where.keys.collect.with_index { |n,i|
            "#{n} = $#{i+t_set.count+1}"
         }.join(' AND ')
      db.norm.cursors.exec(t_sql,i_args.values + t_where.values)
   end
   def delete(**i_where)
      t_sql =
         "delete from  #{name} " +
         'where ' +
         i_where.keys.collect.with_index { |n,i|
            "#{n} = $#{i+1}"
         }.join(' AND ')
      db.norm.cursors.exec(t_sql,i_where.values)
   end
end

end # module NORM


