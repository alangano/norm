
module NORM

class SQLResolver < BasicObject
   def initialize(i_norm)
      @norm = i_norm
   end
   def method_missing(i_method,*i_args,**i_nargs)
      if i_args.count == 0 && i_nargs.count == 0
         @norm.sqls.has_key?(i_method) or
            ::Kernel.raise(
               ::NORM::Error.new("sql '#{i_method}' is not declared")
            )
         @norm.sqls.fetch(i_method)
      elsif(
         i_args.count == 1 &&
         i_method.to_s[-1] == '=' &&
         i_nargs.count == 0
      ) then
         t_sql = i_args.first
         @norm.sql(i_method.to_s[0..-2].to_sym,t_sql)
      else
         super
      end
   end
end

end # module NORM


