
require 'pg'
require_relative 'null_logger'
require_relative 'pg_connection'
require_relative 'cursors'
require_relative 'table'
require_relative 'table_resolver'
require_relative 'sql'
require_relative 'sql_resolver'

module NORM

class Error < Exception
end

class NORM
   attr_reader :db, :sqls
   attr_accessor :logger
   def initialize(i_db)
      @db = i_db
      @logger = NULLLogger.new
      @sqls = {}
   end
   def sql(i_name=nil,i_sql=nil)
      if i_name.nil?
         SQLResolver.new(self)
      else
         if i_sql
            if !@sqls.has_key?(i_name)
               @sqls[i_name] = SQL.new(db,i_sql)
            end
         else
            @sqls.has_key?(i_name) or
               raise Error.new("sql '#{i_name}' has not been declared")
         end
         @sqls[i_name]
      end
   end
#   def sql(i_name,*i_args)
#      @sql[i_name.to_sym].exec(i_args)
#   end
#   def sqldef(i_name,i_sql)
#      @sql[i_name.to_sym] = SQL.new(db,i_sql)
#      self
#   end
   def table(i_name=nil)
      if i_name.nil?
         TableResolver.new(db)
      else
         Table.new(db,i_name)
      end
   end
   def cursors
      instance_variable_defined?('@norm_cursors') or
         @norm_cursors = Cursors.new(self)

      @db.instance_variable_get('@norm_cursors')
   end
end

end

