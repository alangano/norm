Gem::Specification.new do |s|
   s.name         = 'norm'
   s.version      = '0.0.2'
   s.date         = '2020-07-12'
   s.summary      = "No ORM"
   s.description  = "Non-ORM tools for talking to database"
   s.authors      = ["Alan Gano"]
   s.email        = 'alangano@gmail.com'
   s.files        = [
      'lib/norm/pg_connection.rb',
      'lib/norm/table_resolver.rb',
      'lib/norm/norm.rb',
      'lib/norm/table.rb',
      'lib/norm/sql.rb',
      'lib/norm/cursors.rb',
      'lib/norm/null_logger.rb',
      'lib/norm/sql_resolver.rb',
   ]
   s.homepage     = 'https://gitlab.com/alangano/norm'
   s.license      = 'MIT'
end

