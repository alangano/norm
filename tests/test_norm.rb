#!/bin/env ruby
#!/bin/env jruby
$VERBOSE = true

=begin
PG Setup
create user norm encrypted password 'norm';
create database norm owner = norm;
psql -d norm
alter schema public owner to norm;
=end

require_relative '../lib/norm/norm'

ENV['PGHOST']     = 'localhost'
ENV['PGPORT']     = '6666'
ENV['PGDATABASE'] = 'norm'
ENV['PGUSER']     = 'norm'
ENV['PGPASSWORD'] = 'norm'

DB = PG::Connection.new
DB.type_map_for_results = PG::BasicTypeMapForResults.new(DB)
DB.type_map_for_queries = PG::BasicTypeMapForQueries.new(DB)


require 'test/unit'

class Test_NORM < Test::Unit::TestCase
   def setup
      DB.exec 'drop schema public cascade'
      DB.exec 'create schema public'
      DB.exec %q{
         create table test
         (
            id          serial8 primary key,
            name        varchar(30),
            insert_date timestamptz,
            update_date timestamptz,
            status      char(1)
         )
      }
   end
   def teardown
   end
   def test_table_references
      assert_kind_of NORM::Table, DB.norm.table('test')
      assert_kind_of NORM::Table, DB.norm.table('public.test')
      assert_kind_of NORM::Table, DB.norm.table.test
   end
   def test_insert
      DB.norm.table('test').insert(
         name:       'Alan',
         insert_date:Time.now,
         status:     'A'
      )
      DB.norm.table('public.test').insert(
         name:       'Shawn',
         insert_date:Time.now,
         status:     'B'
      )
      assert_kind_of NORM::Table, DB.norm.table.test
      DB.norm.table.test.insert(
         name:       'Mary',
         insert_date:Time.now,
         status:     'C'
      )
   end
   def test_update
      test_insert
      DB.norm.table('test').update(
         status: 'Z',
         update_date: Time.now,
         where: {
            name: 'Alan',
            status: 'A'
         }
      )
   end
   def test_delete
      test_insert
      DB.norm.table('test').delete(
         name: 'Alan',
         status: 'A'
      )
   end
   def test_select
      test_insert
      assert_equal(
         [
            {"name"=>"Alan", "status"=>"A"},
            {"name"=>"Shawn", "status"=>"B"},
            {"name"=>"Mary", "status"=>"C"},
         ],
         DB.norm.table('test').select(:name,:status).map { |r| r }
      )
      assert_equal(
         [
            {"name"=>"Alan", "status"=>"A"},
         ],
         DB.norm.table('test').
            select(
               :name,
               :status,
               where: { status: 'A' }
            ).map { |r| r }
      )
   end
   def test_sql
      test_insert
      DB.exec %q{
         create table test_child
         (
            id          serial8 primary key,
            test_id     int8,
            msg         varchar(30)
         )
      }
      DB.norm.table('test_child').insert(test_id: 1, msg:'test msg 1')
      DB.norm.table('test_child').insert(test_id: 1, msg:'test msg 2')
      DB.norm.table('test_child').insert(test_id: 2, msg:'test msg 3')
      DB.norm.table('test_child').insert(test_id: 2, msg:'test msg 4')

      DB.norm.sql :query1, %q{
         select
            t.id,
            t.name,
            t.status,
            c.msg
         from
            test t
            join test_child c on c.test_id = t.id
      }
      DB.norm.sql 'query2', %q{
         select
            t.id,
            t.name,
            t.status,
            c.msg
         from
            test t
            join test_child c on c.test_id = t.id
         where name = $1
      }
      DB.norm.sql :query3, %q{
         select
            t.id,
            t.name,
            t.status,
            c.msg
         from
            test t
            left outer join test_child c on c.test_id = t.id
      }

      assert_equal(
         [
            {"id"=>1, "name"=>"Alan", "status"=>"A", "msg"=>"test msg 1"},
            {"id"=>1, "name"=>"Alan", "status"=>"A", "msg"=>"test msg 2"},
            {"id"=>2, "name"=>"Shawn", "status"=>"B", "msg"=>"test msg 3"},
            {"id"=>2, "name"=>"Shawn", "status"=>"B", "msg"=>"test msg 4"},
        ],
         DB.norm.sql(:query1).exec.map { |r| r }
      )
      assert_equal(
         [
            {"id"=>1, "name"=>"Alan", "status"=>"A", "msg"=>"test msg 1"},
            {"id"=>1, "name"=>"Alan", "status"=>"A", "msg"=>"test msg 2"},
         ],
         DB.norm.sql('query2').exec('Alan').map { |r| r }
      )
      assert_equal(
         [
            {"id"=>1, "name"=>"Alan", "status"=>"A", "msg"=>"test msg 1"},
            {"id"=>1, "name"=>"Alan", "status"=>"A", "msg"=>"test msg 2"},
            {"id"=>2, "name"=>"Shawn", "status"=>"B", "msg"=>"test msg 3"},
            {"id"=>2, "name"=>"Shawn", "status"=>"B", "msg"=>"test msg 4"},
            {"id"=>3, "name"=>"Mary", "status"=>"C", "msg"=>nil},
         ],
         DB.norm.sql(:query3).exec.map { |r| r }
      )
      DB.norm.sql.query4 = %q{
         select
            t.id,
            t.name,
            t.status,
            c.msg
         from
            test t
            join test_child c on c.test_id = t.id
      }
      DB.norm.sql.query4.exec.map { |r| puts r.inspect }
   end
end

