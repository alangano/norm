
# NORM (No ORM)

NORM, as the name implies, is not an ORM.  Instead, a set of tools to ease your interfaces with the database.

Simple inserts, updates, and deletes are the majority case for DML.  NORM frees the developer from having to handle write the bare SQL, and handle the cursors.

There is no attempt to model the schema.  It does not reference the database, except to excecute SQL as commanded.

For performance, NORM will utilize prepared-cursors where it can. 

Only PostgreSQL is supported at this time.

# Usage

Table are references either by ...
   DB.norm.table('test')
   ... or ...
   DB.norm.table.test

The schema can be specified on the first form:

   DB.norm.table('public.test')

The second form does not allow for schema specifications.

## DML

	DB = PG::Connection.new
	DB.type_map_for_results = PG::BasicTypeMapForResults.new(DB)      
	DB.type_map_for_queries = PG::BasicTypeMapForQueries.new(DB)               
	
	DB.norm.table('test').insert(
	   name:          'Alan',
	   insert_date:   Time.now,
	   status:        'A'
	)
	
	DB.norm.table('test').update(
	   status: 'Z',
	   update_date: Time.now,
	   where: {
	      name:    'Alan',
	      status:  'A'
	   }
	)
	
	DB.norm.table('test').delete(
	   name:    'Alan',
	   status:  'A'
	)


## Queries

Your basic select-list and where-clause, like for DML, is supported.

	DB.norm.table('test').             
	   select(            
	      :name,                                             
	      :status,                        
	      where: { status: 'A' }
	   )
=> PG::Result


## Raw SQL Handling

Declare and run arbitrary SQL.

	DB.norm.sqldef 'query1', %q{
	   select        
	      t.id,                    
	      t.name,        
	      t.status,
	      c.msg
	   from  
	      test t
	      join test_child c on c.test_id = t.id
	}                         
	
	DB.norm.sqldef 'query2', %q{
	   select                                        
	      t.id,                                             
	      t.name,                                           
	      t.status,
	      c.msg    
	   from    
	      test t
	      join test_child c on c.test_id = t.id
	   where name = $1                         
	}                 
	
	DB.norm.sqldef 'query3', %q{
	   select                                        
	      t.id,                                         
	      t.name,                                                       
	      t.status,                                                     
	      c.msg                                                          
	   from                                                              
	      test t                                                         
	      left outer join test_child c on c.test_id = t.id  
	}                                                       
	
	DB.norm.sql('query1')
	=> PG::Result
	
	DB.norm.sql('query2','Alan')
	=> PG::Result
	
	DB.norm.sql('query3')
	=> PG::Result

SQL names can be used in the same manner as table names.

	DB.norm.sql.query4 = %q{                            
	   select                                       
	      t.id,
	      t.name,
	      t.status,
	      c.msg
	   from
	      test t
	      join test_child c on c.test_id = t.id
	}
	DB.norm.sql.query4.exec.map { |r| puts r.inspect }



## Logging Hook

If you set the NORM logger, it will emit some logging.

	logger = Logger.new(STDERR)
	DB.norm.logger = logger

